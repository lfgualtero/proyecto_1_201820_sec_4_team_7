package model.logic;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import model.data_structures.DoublyLinkedList;
import model.vo.VOBikeRoute;

import model.logic.DivvyTripsManager;

class DivvyTripsManagerTest {
	
	private DivvyTripsManager manager;
	
	@Before
	public void setUpEscenario0() {
		manager = new DivvyTripsManager();
	}
	
	@Test
	public void cargarJSONtest() {
		setUpEscenario0();
		manager.loadBikeRoutesJSON("./data/CDOT Bike Routes_2014_1216 - test.json");
		DoublyLinkedList<VOBikeRoute> rutas = manager.getBikeRoutes();
		
		
		//ELEMENTO1
		String p = rutas.getElement(0).getBikeRouteType();
		double p4 = rutas.getElement(0).getLength();
		
		assertEquals("BUFFERED BIKE LANE", p);
		assertEquals(2542.83124955, p4);
		
		//ELEMENTO2
		String q = rutas.getElement(1).getBikeRouteType();
		double q4 = rutas.getElement(1).getLength();
		
		assertEquals("CYCLE TRACK", q);
		assertEquals(241.270022038, q4);

		//ELEMENTO3
		String z = rutas.getElement(2).getBikeRouteType();
		double z4 = rutas.getElement(2).getLength();
		
		assertEquals("BUFFERED BIKE LANE", z);
		assertEquals(1195.00187367, z4);

	}
	
	@Test
	void dummyTest() {
		Integer[] ints = new Integer[10];
		ints[0] = 1;
		ints[1] = 2;
	System.out.println(ints.length);
	}

	
}
