package utils;

import java.util.Comparator;

public enum ComparisonCriteria {
	COMPARE_BIKES_BY_DISTANCE(new BikesComparatorByDistance()),
	COMPARE_BIKES_BY_DURATION(new BikesComparatorByDuration()),
	COMPARE_BIKES_BY_TRIPS(new BikesCompatatorByTrips()),
	COMPARE_TRIPS_BY_START_DATE(new TripsComparatorByStartDate());
	
	private Comparator comp;
	
	private ComparisonCriteria(Comparator comp) {
		this.comp = comp;
	}
	
	public Comparator getComparator() {
		return comp;
	}
	
}
