package api;

import java.time.LocalDateTime;
import java.util.Date;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOTrip;

public interface ITrips {
	/**
	 * REQUIREMENT 1A -Louis
	 * Generar una Cola con todos los viajes de bicicleta que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. El inicio y
	 * terminaci�n del viaje debe estar incluido dentro del periodo de consulta. Los viajes deben
	 * mostrarse en orden cronol�gico de su fecha/hora inicial. 
	 * @param initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @param tripsS 
	 * @return Trip Queue con todos los viajes de bicicleta que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 * COMPLEJIDAD APROX:n^2
	 */
	Queue <VOTrip> getTripsBetweenDates (LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> tripsS) ;
	
	/**
	 * REQUIREMENT 3A -Louis
	 * Mostrar los viajes realizados en una bicicleta, a partir de su identificador, que hayan
	 * iniciado en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora final de
	 * consulta). Los viajes debe estar en orden cronol�gico de su fecha/hora inicial. 
	 * @param bikesIDP identificador de la bicicleta, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Trip Queue de los viajes realizados en una bicicleta, que hayan iniciado en un periodo de tiempo
	 * COMPLEJIDAD APROX: n^2
	 */

	DoublyLinkedList <VOTrip> getABikesTripsBetweenDates (int bikesIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) ;
	
	/**
	 * REQUIREMENT 4A -Louis 
	 * Mostrar la informaci�n completa de los viajes que terminaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). Los viajes deben estar ordenados cronol�gicamente por su fecha/hora
	 * de terminaci�n. 
	 * @param stationIDP identificador de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return identificador del viaje, el identificador de la bicicleta y la fecha/hora terminaci�n
	 * COMPLEJIDAD APROX: n
	 */
	DoublyLinkedList <VOTrip> getTripsInfoBetweenDates (int stationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP);
	
	/**
	 * REQUIREMENT 2B -Andrea
	 * Generar una Cola con todas las estaciones que comenzaron su operaci�n despu�s de
	 * un fecha de consulta dada.
	 * @param initialDateP hora inicial de consulta
	 * @return Cola con todas las estaciones que comenzaron su operaci�n despu�s de
	 * un fecha de consulta dada
	 * COMPLEJIDAD APROX:n
	 */
	Queue <VOTrip> getTripsFromDate (LocalDateTime initialDateP);
	
	/**
	 * REQUIREMENT 4B -Andrea
	 * Mostrar los viajes realizados en una bicicleta, a partir de su identificador, que hayan
	 * tenido una duraci�n menor a un valor que entra como par�metro y que hayan sido
	 * realizados por un hombre o una mujer seg�n lo indique otro par�metro de entrada. Los
	 * viajes debe estar en ORDEN CRONOL�GICO de su fecha/hora inicial. luye el identificador, la
	 * fecha/hora inicial, la fecha/hora final de cada viaje y su duraci�n
	 * @param  bikeIDP el id de la bicicleta, maxTripDuration la duraci�n m�xima del viaje para estar en la lista, genderP el genero de las
	 * personas que realizo los viajes
	 * @return lista de bicicletas a partir de su identificador, que hayan
	 * tenido una duraci�n menor a un valor que entra como par�metro y que hayan sido
	 * realizados por un hombre o una mujer seg�n lo indique otro par�metro de entrada
	 * COMPLEJIDAD APROX: n
	 */
	VOTrip[] getABikeTripsWithShorterDurationThanByGender(int bikeIDP, int maxTripDuration, String genderP, Stack<VOTrip> trips);
	
	
	/**
	 * REQUIREMENT 8 -Andrea
	 * Mostrar la informaci�n completa de los viajes que iniciaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). Los viajes deben estar ordenados cronol�gicamente por su fecha/hora
	 * de inicio.
	 * @param StationIDP id de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Los viajes ordenados cronol�gicamente que iniciaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo 
	 * COMPLEJIDAD APROX: 
	 */
	public VOTrip[] tripsFromStationBetweenDates(int StationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> trips);	
	/**
	 * REQUIREMENT 2C Louis & Andrea
	 * Para una bicicleta, con un id dado, validar si sus viajes en un periodo de tiempo (dado
	 * por una fecha/hora inicial y una fecha/hora final) est�n registrados consistentemente en el
	 * tiempo. Los viajes de la bicicleta deben validarse cronol�gicamente (del m�s antiguo al mas
	 * reciente en el tiempo). Para que un viaje sea validado, la estaci�n de terminaci�n de un
	 * viaje debe ser la estaci�n de inicio del viaje siguiente (en el tiempo).
	 * La validaci�n de cada viaje en el periodo de consulta se va validar usando una pila de
	 * validaci�n. Si la pila esta vac�a, el viaje a validar se acepta como v�lido y se agrega a la pila.
	 * Si la pila No esta vac�a, debe validarse que la estaci�n de terminaci�n del viaje en el tope de
	 * la pila sea la misma estaci�n de inicio del �ltimo viaje que se quiere validar. Si se cumple la
	 * validaci�n se agrega el viaje validado a la pila de validaci�n. Si No se cumple la validaci�n se
	 * detecta una inconsistencia (cronol�gicamente un viaje de la bicicleta No empieza en la
	 * �ltima estaci�n donde termino el �ltimo viaje validado). En este caso, el tope de la pila de
	 * validaci�n (�ltimo viaje validado) y el viaje que gener� la inconsistencia se agregan a una
	 * cola de inconsistencias. Adicionalmente, la pila de validaci�n debe desocuparse
	 * completamente. La validaci�n de los viajes de la bicicleta reinicia con el �ltimo viaje que
	 * gener� la �ltima inconsistencia en la pila de validaci�n.
	 * Al final debe mostrarse los pares de viajes de la bicicleta que quedaron en la cola de
	 * inconsistencias. Por cada inconsistencia detectada incluir: tiempo de terminaci�n y estaci�n
	 * de terminaci�n del viaje validado y tiempo de inicio y estaci�n de inicio del viaje
	 * inconsistente. En caso que la cola de inconsistencias termine vac�a, hay que reportar que
	 * todos los viajes de la bicicleta son consistentes.
	 * @param bikeIDP id de la bicicleta,initialDate , finalDate.
	 * @return  pares de viajes de la bicicleta que quedaron en la cola de
	 * inconsistencias, o true si los viajes son consistentes.  
	 * COMPLEJIDAD APROX: n^2
	 */
	Stack <VOTrip> pilaValidacion(int bikeID, LocalDateTime initialDateP, LocalDateTime finalDateP);
	
	/**
	 * REQUIREMENT 4C Louis & Andrea
	 * Mostrar los viajes que iniciaron y que terminaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). El conjunto de viajes debe estar ordenado
	 * cronol�gicamente por la fecha a la que lleg� o a la que sali� de la estaci�n.
	 * @param StationIDP id de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return lista de viajes que iniciaron y que terminaron en una estaci�n en un periodo de tiempo
	 * COMPLEJIDAD APROX: n^2
	 */
	DoublyLinkedList <VOTrip> tripsStartedAndFinishedAtASationBetweenDates(int stationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP);

}
