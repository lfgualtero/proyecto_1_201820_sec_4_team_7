package api;

import java.time.LocalDateTime;
import java.util.Date;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public interface IBikes {

	DoublyLinkedList <VOTrip> getABikesTripsBetweenDates (int bikesIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) ;

	VOBike[] bikesOrderedByDistanceFromTrips(VOTrip[] trips, Stack<VOStation> estaciones);

	VOBike[] theMostUsedXBikes(int x, Stack<VOTrip> trips, Stack<VOStation> stations);

	DoublyLinkedList<VOBike> getBikesListFromTrips(VOTrip[] tripsList, Stack<VOStation> stationsS );

}
