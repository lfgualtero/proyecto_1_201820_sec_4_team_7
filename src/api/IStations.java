package api;

import java.time.LocalDateTime;
import java.util.Date;

import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOStation;
import model.vo.VOTrip;

public interface IStations {

	
	IDoublyLinkedList<String> getLastNStations(Stack<VOTrip> tripsS, int bikeIdP, int n);
	
	Queue<VOStation> getStationsAfterDate(LocalDateTime date, Stack<VOStation> stations);
}
