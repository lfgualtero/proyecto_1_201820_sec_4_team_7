package api;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBikeRoute;
import model.vo.VOStation;
import model.vo.VOTrip;

public interface IDataManager {
	
	
	Stack<VOStation> loadStations (String stationsFile, Stack<VOStation> stations);
	
	Stack<VOTrip> loadTrips (String tripsFile, Stack<VOTrip> trips); 
	
	DoublyLinkedList<VOBikeRoute> loadBikeRoutesJSON(String jsonRoute);
	



	
	
}
	
