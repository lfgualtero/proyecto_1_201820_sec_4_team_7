package model.logic;

import java.time.LocalDateTime;
import api.ITrips;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOTrip;
import utils.ComparisonCriteria;
import utils.Ordenador;
import utils.SortingAlgorithms;
import utils.TripsComparatorByStartDate;

public class Trips implements ITrips{


	@Override
	public Queue<VOTrip> getTripsBetweenDates(LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> tripsS) {
		Queue <VOTrip> respuesta=new Queue<VOTrip>();

		for(VOTrip tempTrip : tripsS) {
			if(tempTrip.getStart_time().isAfter(initialDateP) && tempTrip.getEnd_time().isBefore(finalDateP)) 
			{

				respuesta.enqueue(tempTrip);
			}
		}
		return respuesta;
	}

	@Override
	public DoublyLinkedList<VOTrip> getABikesTripsBetweenDates(int bikesIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DoublyLinkedList<VOTrip> getTripsInfoBetweenDates(int stationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOTrip> getTripsFromDate(LocalDateTime initialDateP) {
		// TODO Auto-generated method stub
		return null;
	}

	//Retorna null si nungun elemento cumple con el criterio de busqueda
	@Override
	public VOTrip[] getABikeTripsWithShorterDurationThanByGender(int bikeIDP, int maxTripDuration,
			String genderP, Stack<VOTrip> trips) {
		VOTrip[] respuesta = null;
		DoublyLinkedList<VOTrip> viajesBici = new DoublyLinkedList<VOTrip>();
		
		for(VOTrip tripTemp : trips) {
			int id= tripTemp.getBikeid();
			int time = tripTemp.getTripSeconds();
			String gen = tripTemp.getGender();
			if(id == bikeIDP && time < maxTripDuration && gen.equalsIgnoreCase(genderP) ) {
				viajesBici.add(tripTemp);
			}
		}
		
		if(viajesBici.getSize() > 0) {
			Ordenador<VOTrip> o = new Ordenador<VOTrip>();
			respuesta = viajesBici.toArray(VOTrip.class);
			o.ordenar(SortingAlgorithms.MERGESORT, true, respuesta, 
					(TripsComparatorByStartDate)ComparisonCriteria.COMPARE_TRIPS_BY_START_DATE.getComparator());
		}
		
		return respuesta;
	
	}

	@Override
	public VOTrip[] tripsFromStationBetweenDates(int StationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP, Stack<VOTrip> trips) {
		VOTrip[] listaRespuesta = null;

		DoublyLinkedList<VOTrip> choosenTrips = new DoublyLinkedList<VOTrip>();
		for(VOTrip t : trips) {
			if(t.getFrom_station_id() == StationIDP && t.getStart_time().isAfter(initialDateP) && t.getEnd_time().isBefore(finalDateP)) {
				choosenTrips.add(t);
			}
		}
		
		if(choosenTrips.getSize() > 0) {
			Ordenador<VOTrip> o = new Ordenador<VOTrip>();
			listaRespuesta = choosenTrips.toArray(VOTrip.class);
			o.ordenar(SortingAlgorithms.MERGESORT, true, listaRespuesta, 
					(TripsComparatorByStartDate)ComparisonCriteria.COMPARE_TRIPS_BY_START_DATE.getComparator());
		}
		
		return listaRespuesta;
	
	}

	@Override
	public Stack<VOTrip> pilaValidacion(int bikeID, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DoublyLinkedList<VOTrip> tripsStartedAndFinishedAtASationBetweenDates(int stationIDP, LocalDateTime initialDateP,
			LocalDateTime finalDateP) {
		// TODO Auto-generated method stub
		return null;
	}

}
