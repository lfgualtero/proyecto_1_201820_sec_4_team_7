package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import api.IDataManager;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBikeRoute;
import model.vo.VOStation;
import model.vo.VOTrip;

public class DataManager implements IDataManager {


	@Override
	public Stack<VOStation> loadStations( String stationsFile, Stack<VOStation> stationsS2) 
	{
		Stack<VOStation> stationsS = stationsS2;
		String csvFile = stationsFile;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try 
		{
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();


			while ((line = br.readLine()) != null)
			{   

				String[] datos = line.split(cvsSplitBy);

				int id;
				String name;
				String city;
				double latitude;
				double longitude;
				int dpcapacity;
				String online_date;

				try 
				{
					if(line.startsWith("\"")) 
					{

						id=Integer.parseInt(datos[0].substring(1, datos[0].length()-1));
						name=datos[1].substring(1, datos[1].length()-1);
						city=datos[2].substring(1, datos[2].length()-1);
						latitude=Double.parseDouble(datos[3].substring(1, datos[3].length()-1));
						longitude=Double.parseDouble(datos[4].substring(1, datos[4].length()-1));
						dpcapacity=Integer.parseInt(datos[5].substring(1, datos[5].length()-1));
						online_date=datos[6].substring(1, datos[6].length()-1);

						String[] horaYFecha = online_date.split(" ");
						String fecha = horaYFecha[0];
						String hora = horaYFecha[1];
						LocalDateTime date = convertirFecha_Hora_LDT(fecha, hora);

						stationsS.push(new VOStation( id, name,  city, latitude, longitude,  dpcapacity, date ));

					}else 
					{

						id=Integer.parseInt(datos[0]);
						name=datos[1];
						city=datos[2];
						latitude=Double.parseDouble(datos[3]);
						longitude=Double.parseDouble(datos[4]);
						dpcapacity=Integer.parseInt(datos[5]);
						online_date=datos[6];

						String[] horaYFecha = online_date.split(" ");
						String fecha = horaYFecha[0];
						String hora = horaYFecha[1];
						LocalDateTime date = convertirFecha_Hora_LDT(fecha, hora);

						stationsS.push(new VOStation(id, name,  city, latitude, longitude,  dpcapacity, date ));

					}

				} catch (NumberFormatException e) 
				{
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return stationsS;
	}




	@Override
	public Stack<VOTrip> loadTrips(String tripsFile, Stack<VOTrip> trips) {

		Stack<VOTrip> tripsSDataM= trips;

		String csvFile = tripsFile;
		BufferedReader br = null;
		String line = "";
		//Se define separador ","

		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			br.readLine();

			int trip_id;
			LocalDateTime start_time;
			LocalDateTime end_time;
			int bikeid;
			int tripduration;
			int from_station_id;
			String from_station_name;
			int to_station_id;
			String to_station_name;
			String usertype;
			String gender;
			int birthyear;

			while ((line = br.readLine()) != null) { 

				String[] datos = line.split(cvsSplitBy);

				try {

					if(line.startsWith("\"")) 
					{

						trip_id=Integer.parseInt(datos[0].substring(1, datos[0].length()-1));



						String start=datos[1].substring(1, datos[1].length()-1);
						String startD=start.split("\\s+")[0];
						String startH=start.split("\\s+")[1];
						start_time=convertirFecha_Hora_LDT(startD, startH);


						String end=datos[2].substring(1, datos[2].length()-1);
						String endD=end.split("\\s+")[0];
						String endH=end.split("\\s+")[1];
						end_time=convertirFecha_Hora_LDT(endD, endH);


						bikeid=Integer.parseInt(datos[3].substring(1, datos[3].length()-1));
						tripduration=Integer.parseInt(datos[4].substring(1, datos[4].length()-1));
						from_station_id=Integer.parseInt(datos[5].substring(1, datos[5].length()-1));
						from_station_name=datos[6].substring(1, datos[6].length()-1);
						to_station_id=Integer.parseInt(datos[7].substring(1, datos[7].length()-1));
						to_station_name=datos[8].substring(1, datos[8].length()-1);
						usertype=datos[9].substring(1, datos[9].length()-1);

						if(datos[10].equals("\""+"\"") && datos[11].equals("\""+"\"")) {

							gender=" ";
							birthyear=0;

						}else if(datos[10].equals("\""+"\"")) {

							gender=" ";
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}else if(datos[11].equals("\""+"\"")) {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear=0 ;

						}else {

							gender=datos[10].substring(1, datos[10].length()-1);
							birthyear= Integer.parseInt(datos[11].substring(1, datos[11].length()-1));

						}

						VOTrip v=new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear);
						tripsSDataM.push(v);

					}else 
					{


						trip_id=Integer.parseInt(datos[0]);

						String start1=datos[1];
						String startD1=start1.split("\\s+")[0];
						String startH1=start1.split("\\s+")[1];
						start_time=convertirFecha_Hora_LDT(startD1, startH1);


						String end1=datos[2];
						String endD1=end1.split("\\s+")[0];
						String endH1=end1.split("\\s+")[1];
						end_time=convertirFecha_Hora_LDT(endD1, endH1);



						bikeid=Integer.parseInt(datos[3]);
						tripduration=Integer.parseInt(datos[4]);
						from_station_id=Integer.parseInt(datos[5]);
						from_station_name=datos[6];
						to_station_id=Integer.parseInt(datos[7]);
						to_station_name=datos[8];
						usertype=datos[9];

						if(datos.length==10)
						{
							gender=" ";
							birthyear=0;

						}
						else if(datos.length==11)
						{
							gender=datos[10];
							birthyear=0;

						}
						else 
						{
							gender=datos[10];
							birthyear=Integer.parseInt(datos[11]);

						}

						VOTrip v = new VOTrip(trip_id,start_time ,end_time , bikeid,tripduration ,from_station_id ,from_station_name, to_station_id, to_station_name,usertype, gender, birthyear);
						tripsSDataM.push(v);

					}

				} catch (NumberFormatException e) {

					e.printStackTrace();

				} 
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return tripsSDataM;
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);

		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = 00;
		
		if(datosHora.length ==3) {
			segundos = Integer.parseInt(datosHora[2]);
		}

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

	@Override
	public DoublyLinkedList<VOBikeRoute> loadBikeRoutesJSON(String jsonRoute) {
		DoublyLinkedList<VOBikeRoute> routes = new DoublyLinkedList<VOBikeRoute>();
		FileReader file;
		try {
			file = new FileReader(jsonRoute);
			JSONParser parser = new JSONParser();
			JSONObject js = (JSONObject) parser.parse(file);

			System.out.println(js.size());
			JSONArray rutasJs = (JSONArray) js.get("data"); // rutas: la lista de rutas que se encuentran en data
			routes = loadArrayOfRoutesJSON(rutasJs); // Actualiza routes con todas las rutas del JSON

			file.close();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return routes;
	}

	/**
	 * Metodo auxiliar de loadBikeRoutesJSON()
	 * Carga todas las rutas del JSONArray que se pasa por parametro en una lista encadenada
	 * @Return retorna una lista encadenada con las rutas cargadas
	 */
	private DoublyLinkedList<VOBikeRoute> loadArrayOfRoutesJSON(JSONArray rutas) {
		DoublyLinkedList<VOBikeRoute> VOBikeRoutestArray = new DoublyLinkedList<VOBikeRoute>();
		if(rutas != null) {
			for(int i = 0; i < rutas.size(); i++) {
				JSONArray bikeRoute = (JSONArray) rutas.get(i);
				VOBikeRoute loadedRoute = loadRouteDataJSON(bikeRoute);
				VOBikeRoutestArray.add(loadedRoute);
			}
		}
		return VOBikeRoutestArray;
	}
	/**
	 * Metodo auxiliar de loadArrayOfRoutesJSON(JSONArray rutas)
	 * Lee los datos de una ruta del JSON Array que se pasa por parametro 
	 * @Return Un objeto de tipo VOBikeRoute con todos los datos necesarios de la ruta, o NULL si no pudo leer rutaData
	 */
	private VOBikeRoute loadRouteDataJSON(JSONArray rutaData) {
		VOBikeRoute VO = null;

		if(rutaData != null && rutaData.size() > 0) {
			String type = (String) rutaData.get(8);
			String location = (String) rutaData.get(9);
			String referenceStreet = (String) rutaData.get(11);
			String endStreet1 = (String) rutaData.get(12);
			String endStreet2 = (String) rutaData.get(13);
			String lenght = (String) rutaData.get(15);
			VO = new VOBikeRoute(type, location, referenceStreet, endStreet1, endStreet2, Double.valueOf(lenght));

			/** ORDEN DE PARAMETROS
			 *  this.bikeRouteType = bikeRouteType;
					this.location = route;
					this.referenceStreet = referenceStreet;
					this.endStreet1 = endStreet1;
					this.endStreet2 = endStreet2;
					this.length = length;
			 */
		}
		return VO;
	}



}
