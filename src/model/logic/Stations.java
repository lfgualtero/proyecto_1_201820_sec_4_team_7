package model.logic;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Iterator;

import api.IStations;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOStation;
import model.vo.VOTrip;

public class Stations implements IStations{
	
	public VOStation buscarEstacionesPorNombre(Stack <VOStation> stations, String pName){
		VOStation respuesta=null;
		Iterator <VOStation> iter= stations.iterator();
		while(iter.hasNext() && respuesta==null){
			VOStation station= iter.next();
			if(pName.equals(station.darName())){
				respuesta=station;
			}
		}
		return respuesta;
	}

	@Override
	public IDoublyLinkedList<String> getLastNStations(Stack<VOTrip> tripsS, int bikeIdP, int n) {
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Queue<VOStation> getStationsAfterDate(LocalDateTime date, Stack<VOStation> stations) {
		Queue<VOStation> estaciones = new Queue<VOStation>(); 
		for(VOStation temp : stations) {
			if(temp.darOnline_date().compareTo(date) > 0) {
				estaciones.enqueue(temp);
			}
		}
		return estaciones;
	}


}
