package model.logic;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Date;
import api.IBikes;
import api.IDivvyTripsManager;
import api.IStations;
import api.ITrips;
import model.vo.VOBike;
import model.vo.VOBikeRoute;
import model.vo.VOStation;
import model.vo.VOTrip;
import utils.BikesCompatatorByTrips;
import utils.ComparisonCriteria;
import utils.Ordenador;
import utils.SortingAlgorithms;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IteratorList;
import model.data_structures.Queue;
import model.data_structures.Stack;



public class DivvyTripsManager implements IDivvyTripsManager {
	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------
	
	/**
	 * Stack de stations
	 */
	private Stack<VOStation> stationsS;

	/**
	 * Stack de trips
	 */
	private Stack<VOTrip> tripsS;

	/**
	 * Lista de rutas
	 */
	private DoublyLinkedList<VOBikeRoute> routes;
	
	/*
	 * Administrador de Datos
	 */
	private DataManager dataManager;
	
	/*
	 * Administrador de consutas sobre viajes
	 */
	private	Trips tripsM;

	/*
	 * Administrador de consutas sobre bicicletas
	 */
	private	Bikes bikesM;

	/*
	 * Administrador de consutas sobre estaciones
	 */
	private	Stations stationsM;

	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------
	public DivvyTripsManager() {
		super();
		stationsS= new Stack<VOStation>();
		
		//( del mas viejo almas reciente)
		tripsS=new Stack<VOTrip>();
		
		routes=new DoublyLinkedList<VOBikeRoute>();
		
		
		dataManager = new DataManager();
		tripsM = new Trips();
		stationsM = new Stations();
		tripsM = new Trips();
		bikesM = new Bikes();
		
	}
	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------
	
	/**
	 * Carga los viajes
	 * @param  ubicacion del archivo.
	 */
	@Override
	public void loadTrips(String tripsFile) {
		if(tripsFile.equals(""))
			tripsS = new Stack<VOTrip>();
		else
			tripsS = dataManager.loadTrips(tripsFile, tripsS);
	}

	/**
	 * Carga las estaciones
	 * @param  ubicacion del archivo.
	 */
	@Override
	public void loadStations(String stationsFile) {
		if(stationsFile.equals(""))
			stationsS = new Stack<VOStation>();
		else
			stationsS = dataManager.loadStations(stationsFile, stationsS);
	}

	/**
	 * Carga las ciclorutas de las bicicletas, las rutas deben cargarse en una lista
	 * en el orden que aparecen en el archivo.
	 * @param bikeRoutesFile ubicacion del archivo.
	 */
	@Override
	public void loadBikeRoutesJSON(String jsonRoute) {
		routes = dataManager.loadBikeRoutesJSON(jsonRoute);
	}
	
	/**
	 * REQUIREMENT 1 -Louis
	 * Generar una Cola con todos los viajes que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta. El inicio y
	 * terminaci�n del viaje debe estar incluido dentro del periodo de consulta.
	 *  
	 * Los viajes deben mostrarse en orden cronol�gico de su fecha/hora inicial. Incluye el identificador del viaje,
	 * el identificador de la bicicleta, la fecha/hora inicial y la fecha/hora final de cada viaje.
	 * 
	 * YA EST� ORDENADA DEL MAS VIEJO AL MAS RECIENTE!!! A PARTIR DEL ORDEN EN QUE EST�N CARGADOS LOS TRIPS EN tripS.
	 *(tambien mas viejo al mas reciente)
	 * 
	 * @param initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Trip Queue con todos los viajes de bicicleta que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 */
	public Queue <VOTrip> getTripsBetweenDates (LocalDateTime initialDateP, LocalDateTime finalDateP) {
		return tripsM.getTripsBetweenDates(initialDateP, finalDateP, tripsS);
	}


	/**
	 * REQUIREMENT 2 -Louis
	 * Mostrar las bicicletas ordenadas de mayor a menor por el n�mero de viajes realizados
	 * en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 * Incluye el identificador de la bicicleta, el total de viajes realizados y la distancia total
	 * recorrida por bicicleta.
	 * @param initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Bike Queue con todos las bicicletas que se prestaron en un periodo de
	 * tiempo dado por una fecha/hora inicial y una fecha/hora final de consulta.
	 */
	public DoublyLinkedList<VOBike> getBikesBetweenDates (LocalDateTime initialDateP, LocalDateTime finalDateP) {
		
				Queue <VOTrip> tripsQueue=getTripsBetweenDates ( initialDateP,  finalDateP);
				VOTrip[] tripsArr= new VOTrip[tripsQueue.size()];
				int i=0;
				for(VOTrip tripTemp: tripsQueue) {
					tripsArr[i]=tripTemp;
					i++;
				}
				DoublyLinkedList<VOBike> bikesDO= bikesM.getBikesListFromTrips(tripsArr, stationsS);
				VOBike[] bikesArr= new VOBike[bikesDO.getSize()];
				i=0;
				for(VOBike bikeTemp: bikesDO) {
					bikesArr[i]=bikeTemp;
					i++;
				}
				
				Ordenador<VOBike> o = new Ordenador<VOBike>();
				o.ordenar(SortingAlgorithms.QUICKSORT, false, bikesArr, 
				(BikesCompatatorByTrips)ComparisonCriteria.COMPARE_BIKES_BY_TRIPS.getComparator());
				
				i=0;
				bikesDO=new DoublyLinkedList<VOBike>();
				
				for (i = 0; i < bikesArr.length; i++) {
					bikesDO.addAtEnd(bikesArr[i]);
				}
				
				return bikesDO;
		 	}
	

	/**
	 * REQUIREMENT 3 -Louis
	 * Mostrar los viajes realizados en una bicicleta, a partir de su identificador, que hayan
	 * iniciado en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora final de
	 * consulta). Los viajes debe estar en orden cronol�gico de su fecha/hora inicial. Incluye el
	 * identificador, la fecha/hora inicial y la fecha/hora final de cada viaje.
	 * @param bikesIDP identificador de la bicicleta, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Trip Queue de los viajes realizados en una bicicleta, que hayan iniciado en un periodo de tiempo
	 */

	public DoublyLinkedList<VOTrip> getABikesTripsBetweenDates (int bikeIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		Queue <VOTrip> tripsQueue=getTripsBetweenDates ( initialDateP,  finalDateP);
		
		DoublyLinkedList<VOTrip> respuesta= new DoublyLinkedList<VOTrip>();
		
		for(VOTrip tempTrip: tripsQueue) {
			
			if(tempTrip.getBikeid()==bikeIDP) {
				respuesta.addAtEnd(tempTrip);
		}
			
		}
		return respuesta;

	}
	
	/**
	 * REQUIREMENT 4 -Louis 
	 * Mostrar la informaci�n completa de los viajes que terminaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). Los viajes deben estar ordenados cronol�gicamente por su fecha/hora
	 * de terminaci�n. Por cada viaje incluir su identificador, el identificador de la bicicleta y la 
	 * fecha/hora terminaci�n.
	 * @param stationIDP identificador de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return identificador del viaje, el identificador de la bicicleta y la fecha/hora terminaci�n
	 */
		public DoublyLinkedList<VOTrip> getTripsInfoBetweenDatesSameToStation (int stationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		
				Queue <VOTrip> tripsQueue=getTripsBetweenDates ( initialDateP,  finalDateP);
		
				DoublyLinkedList<VOTrip> respuesta= new DoublyLinkedList<VOTrip>();
		
				for(VOTrip tempTrip: tripsQueue) {
		
					if(tempTrip.getTo_station_id()==stationIDP) {
						respuesta.addAtEnd(tempTrip);
					}
		
				}
				return respuesta;
		 	}
	
	/**
	 * REQUIREMENT 5 -Andrea
	 * Generar una Cola con todas las estaciones que comenzaron su operaci�n despu�s de
	 * un fecha de consulta dada.
	 * @param initialDateP hora inicial de consulta
	 * @return Cola con todas las estaciones que comenzaron su operaci�n despu�s de
	 * un fecha de consulta dada
	 * @post 
	 */
	@Override 
	public Queue<VOStation> getStationsFromDate(LocalDateTime initialDateP) {
		return stationsM.getStationsAfterDate(initialDateP, stationsS);
	}


	/**
	 * REQUIREMENT 6 -Andrea
	 * Mostrar las bicicletas ordenadas de MAYOR A MENOR por la distancia total recorrida en
	 * sus viajes en un periodo de tiempo dado por una fecha/hora inicial y una fecha/hora final 
	 * de consulta. Incluye el identificador de la bicicleta, la distancia total recorrida por bicicleta
	 * y el total de viajes realizados.
	 * @param initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return lista de bicicletas ordenadas de mayor a menor por la distancia total recorrida en
	 * sus viajes en un periodo de tiempo dado
	 * @post 
	 */ 
	public VOBike[] bikesOrderedByDistanceBetweenDates(LocalDateTime initialDateP, LocalDateTime finalDateP){
		VOTrip[] trips = tripsM.getTripsBetweenDates(initialDateP, finalDateP, tripsS).toArray(VOTrip.class);
		return bikesM.bikesOrderedByDistanceFromTrips(trips, stationsS);
	}

	/**
	 * REQUIREMENT 7 -Andrea
	 * Mostrar los viajes realizados en una bicicleta, a partir de su identificador, que hayan
	 * tenido una duraci�n menor a un valor que entra como par�metro y que hayan sido
	 * realizados por un hombre o una mujer seg�n lo indique otro par�metro de entrada. Los
	 * viajes debe estar en ORDEN CRONOL�GICO de su fecha/hora inicial. Incluye el identificador, la
	 * fecha/hora inicial, la fecha/hora final de cada viaje y su duraci�n.
	 * @param  bikeIDP el id de la bicicleta, maxTripDuration la duraci�n m�xima del viaje para estar en la lista, genderP el genero de las
	 * personas que realizo los viajes
	 * @return lista de bicicletas a partir de su identificador, que hayan
	 * tenido una duraci�n menor a un valor que entra como par�metro y que hayan sido
	 * realizados por un hombre o una mujer seg�n lo indique otro par�metro de entrada
	 * @post 
	 */
	public VOTrip[] getABikeTripsWithShorterDurationThanByGender(int bikeIDP, int maxTripDuration, String genderP){
		return tripsM.getABikeTripsWithShorterDurationThanByGender(bikeIDP, maxTripDuration, genderP, tripsS);
	}
	/**
	 * REQUIREMENT 2C Louis & Andrea
	 * Para una bicicleta, con un id dado, validar si sus viajes en un periodo de tiempo (dado
	 * por una fecha/hora inicial y una fecha/hora final) est�n registrados consistentemente en el
	 * tiempo. Los viajes de la bicicleta deben validarse cronol�gicamente (del m�s antiguo al mas
	 * reciente en el tiempo). Para que un viaje sea validado, la estaci�n de terminaci�n de un
	 * viaje debe ser la estaci�n de inicio del viaje siguiente (en el tiempo).
	 * La validaci�n de cada viaje en el periodo de consulta se va validar usando una pila de
	 * validaci�n. Si la pila esta vac�a, el viaje a validar se acepta como v�lido y se agrega a la pila.
	 * Si la pila No esta vac�a, debe validarse que la estaci�n de terminaci�n del viaje en el tope de
	 * la pila sea la misma estaci�n de inicio del �ltimo viaje que se quiere validar. Si se cumple la
	 * validaci�n se agrega el viaje validado a la pila de validaci�n. Si No se cumple la validaci�n se
	 * detecta una inconsistencia (cronol�gicamente un viaje de la bicicleta No empieza en la
	 * �ltima estaci�n donde termino el �ltimo viaje validado). En este caso, el tope de la pila de
	 * validaci�n (�ltimo viaje validado) y el viaje que gener� la inconsistencia se agregan a una
	 * cola de inconsistencias. Adicionalmente, la pila de validaci�n debe desocuparse
	 * completamente. La validaci�n de los viajes de la bicicleta reinicia con el �ltimo viaje que
	 * gener� la �ltima inconsistencia en la pila de validaci�n.
	 * Al final debe mostrarse los pares de viajes de la bicicleta que quedaron en la cola de
	 * inconsistencias. Por cada inconsistencia detectada incluir: tiempo de terminaci�n y estaci�n
	 * de terminaci�n del viaje validado y tiempo de inicio y estaci�n de inicio del viaje
	 * inconsistente. En caso que la cola de inconsistencias termine vac�a, hay que reportar que
	 * todos los viajes de la bicicleta son consistentes.
	 * @param bikeIDP id de la bicicleta,initialDate , finalDate.
	 * @return  pares de viajes de la bicicleta que quedaron en la cola de
	 * inconsistencias, o true si los viajes son consistentes. 
	 */
	public Queue<VOTrip> pilaValidacion(int bikeIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		Stack<VOTrip> pilaViajesValidos=new Stack<VOTrip>();
		Queue <VOTrip> inconsistencias=new Queue<VOTrip>();

		DoublyLinkedList<VOTrip>losViajesDeBici=getABikesTripsBetweenDates (bikeIDP,initialDateP, finalDateP);

		IteratorList<VOTrip>it=(IteratorList<VOTrip>) losViajesDeBici.iterator();

		VOTrip temp=null;
		while (it.hasNext()) {

			VOTrip trip =  it.next();

			//Si la pila esta vac�a, el viaje a validar se acepta como v�lido y se agrega a la pila.
			if(!pilaViajesValidos.isEmpty()){

				//Si la pila No esta vac�a, debe validarse que la estaci�n de terminaci�n del viaje en el tope de
				//la pila sea la misma estaci�n de inicio del �ltimo viaje que se quiere validar
				int EndId=pilaViajesValidos.peek().getTo_station_id();
				if(EndId==trip.getFrom_station_id()) {

					//Si se cumple la validaci�n se agrega el viaje validado a la pila de validaci�n.
					//temp para evitar que se pierda le trip en el linked list, proposito: la llamada a 
					//previous que se hace cuando el viaje es inconcistente. Si se mantiene la referencia
					// no va a lanzar un null pointer
					temp=trip;
					pilaViajesValidos.push(temp);
				}else {
					//El tope de la pila de validaci�n (�ltimo viaje validado) y el viaje que gener� la inconsistencia 
					//se agregan a una cola de inconsistencias.
					inconsistencias.enqueue(pilaViajesValidos.pop());
					inconsistencias.enqueue(trip);
					pilaViajesValidos=new Stack<VOTrip>();
					//La validaci�n de los viajes de la bicicleta reinicia con el �ltimo viaje que
					//gener� la �ltima inconsistencia en la pila de validaci�n. Un previous tiene sentido porque 
					// cuando empiece el while se cancela y empieza en el mismo trip en el que termino esta vuelta.
					it.previous();
				}

			}else {
				pilaViajesValidos.push(trip);
			}
		}

		return inconsistencias;
	}
	/**
	 * REQUIREMENT 8 -Andrea
	 * Mostrar la informaci�n completa de los viajes que iniciaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). Los viajes deben estar ordenados cronol�gicamente por su fecha/hora
	 * de inicio. Por cada viaje incluir su identificador, el identificador de la bicicleta y la fecha/hora
	 * inicial.
	 * @param StationIDP id de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return Los viajes ordenados cronol�gicamente que iniciaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo 
	 * @post 
	 */
	public VOTrip[] tripsFromStationBetweenDates(int StationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP){
		return tripsM.tripsFromStationBetweenDates(StationIDP, initialDateP, finalDateP, tripsS);
	}
	
	/**
	 * REQUIREMENT 3C Louis & Andrea
	 * Mostrar las X bicicletas que m�s hayan sido usadas seg�n la duraci�n total de sus viajes
	 * (ordenadas de mayor a menor duraci�n de uso). El valor X es un par�metro de entrada.
	 * Incluye el identificador de la bicicleta y la duraci�n total de sus viajes. 
	 * @param X numero de bicicletas que mas hayan sido usadas
	 * @return X bicicletas que m�s hayan sido usadas seg�n la duraci�n total de sus viajes
	 */
	public VOBike[] theMostUsedXBikes(int x){
		return bikesM.theMostUsedXBikes(x, tripsS, stationsS);
	}
	/**
	 * REQUIREMENT 4C Louis & Andrea
	 * Mostrar los viajes que iniciaron y que terminaron en una estaci�n (dado su
	 * identificador) en un periodo de tiempo (dado por una fecha/hora inicial y una fecha/hora
	 * final de consulta). Para los viajes que iniciaron hay que indicar que es un viaje de inicio, su
	 * identificador, el identificador de la bicicleta y su fecha/hora de inicio. Para los viajes que
	 * terminaron hay que indicar que es un viaje de terminaci�n, su identificador, el identificador
	 * de la bicicleta y su fecha/hora de terminaci�n. El conjunto de viajes debe estar ordenado
	 * cronol�gicamente por la fecha a la que lleg� o a la que sali� de la estaci�n.
	 * @param StationIDP id de la estacion, initialDateP hora inicial de consulta,finalDateP hora final de consulta
	 * @return lista de viajes que iniciaron y que terminaron en una estaci�n en un periodo de tiempo
	 */
	public Object[] tripsStartedAndFinishedAtASationBetweenDates(int stationIDP, LocalDateTime initialDateP, LocalDateTime finalDateP){
		
		Queue <VOTrip> tripsQueue=getTripsBetweenDates ( initialDateP,  finalDateP);
		DoublyLinkedList<VOTrip> listaStart=new  DoublyLinkedList<>();
		DoublyLinkedList<VOTrip> listaEnd=new  DoublyLinkedList<>();
		Object[] respuesta= {listaStart, listaEnd};

		for(VOTrip tempTrip: tripsQueue) {

			if(tempTrip.getTo_station_id()==stationIDP) {
				
				listaEnd.add(tempTrip);
			}else if(tempTrip.getFrom_station_id()==stationIDP) {
				
				listaStart.add(tempTrip);
			}

		}
		return respuesta;
	}
	
	public DoublyLinkedList<VOBikeRoute> getBikeRoutes(){
		return routes;
	}
	
	
	/** (non-Javadoc)
	 * @see api.IDivvyTripsManager#quickSortTrips(model.vo.VOTrip[], boolean)
	 */
	public void quickSortTrips(VOTrip[] elementos, boolean asc, Comparator<VOTrip> comp) {
		Ordenador<VOTrip> o = new Ordenador<VOTrip>();
		o.ordenar(SortingAlgorithms.QUICKSORT, asc, elementos, comp);
	}
	
	//--------------------------------------------------------
	//Getters
	//--------------------------------------------------------


	public int getTripsSSize(){
		return tripsS.size();
	}
	
	public Stack <VOStation> darStationsS(){
		return stationsS;
	}



	
}
