package model.logic;

import java.time.LocalDateTime;
import api.IBikes;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;
import utils.BikesComparatorByDistance;
import utils.BikesComparatorByDuration;
import utils.ComparisonCriteria;
import utils.Ordenador;
import utils.SortingAlgorithms;

public class Bikes implements IBikes{

	private static final int EARTH_RADIUS = 6371;


	@Override
	public DoublyLinkedList<VOTrip> getABikesTripsBetweenDates(int bikesIDP, LocalDateTime initialDateP, LocalDateTime finalDateP) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public VOBike[] theMostUsedXBikes(int x, Stack<VOTrip> trips, Stack<VOStation> stations) {
		VOBike[] retornar = new VOBike[x];
		
		DoublyLinkedList<VOBike> bikes = getBikesListFromTripsStack(trips, stations);
		VOBike[] odenadasMayorMenor = bikes.toArray(VOBike.class);
		Ordenador<VOBike> o = new Ordenador<VOBike>();
		o.ordenar(SortingAlgorithms.MERGESORT, false, odenadasMayorMenor, (BikesComparatorByDuration)ComparisonCriteria.COMPARE_BIKES_BY_DURATION.getComparator());

		int i = 0;
		while(i < x) {
			retornar[i] = odenadasMayorMenor[i];
			i++;
		}
		return retornar;
	}
	


	/**
	 * revisa si una bicicleta esta en una lista de bicicletas
	 * @param id de la bicicleta de la que se quiere saber si esta en la lista o no
	 * @param listaBicis lista por revisar
	 * @return VOBike si esta en la lista, null si el bike no esta en la lista
	 */
	private VOBike getBike(int id, DoublyLinkedList<VOBike> listaBicis) {
		VOBike bike = null;
		if(!listaBicis.isEmpty()) {
			for(VOBike temp : listaBicis) {

				if(temp != null) {

					if(temp.darId() == id) {
						bike = temp;
						break;
					}
				}
			}
		}
		return bike;
	}


	/**
	 * Retorna una lista enlazada con las bicicletas que se encontraban en la lista de viajes
	 * @param tripsList : la lista de viajes de donde se van a sacar las bicis
	 * @param stationsS : la Pila de estaciones para hallar la distancia por su latitud y longitud
	 * @return una lista con las bicletas
	 */
	public DoublyLinkedList<VOBike> getBikesListFromTrips(VOTrip[] tripsList, Stack<VOStation> stationsS ) {

		DoublyLinkedList<VOBike> bikesFromTrips = new DoublyLinkedList<VOBike>();

		if(tripsList.length != 0) {
			for(VOTrip viajeTemp : tripsList) {
				int estacionInicioId = viajeTemp.getFrom_station_id();
				int estacionFinalId = viajeTemp.getTo_station_id();

				int duracionViaje = viajeTemp.getTripSeconds();
				int id = viajeTemp.getBikeid();
				double distaciaCalculada = calcularDistaciaConIdEstaciones(estacionInicioId, estacionFinalId, stationsS);

				VOBike existe = getBike(id, bikesFromTrips); //SE REVISA SI LA BICICLETA YA EXISTE EN LA LISTA

				if(existe != null) {
					existe.addDistanceToTotalDistancia(distaciaCalculada);
					existe.sumarADuracionViajes(duracionViaje);
					existe.addOneTotalViajes();
				}
				else {
					VOBike bike=new VOBike(id);
					bike.addDistanceToTotalDistancia(distaciaCalculada);
					bike.sumarADuracionViajes(duracionViaje);
					bike.addOneTotalViajes();
					bikesFromTrips.add(bike);
				}
			}
		}

		return bikesFromTrips;
	}


	public DoublyLinkedList<VOBike> getBikesListFromTripsStack(Stack<VOTrip> tripsList, Stack<VOStation> stationsS ) {

		DoublyLinkedList<VOBike> bikesFromTrips = new DoublyLinkedList<VOBike>();

		if(!tripsList.isEmpty()) {
			for(VOTrip viajeTemp : tripsList) {
				int estacionInicioId = viajeTemp.getFrom_station_id();
				int estacionFinalId = viajeTemp.getTo_station_id();

				int duracionViaje = viajeTemp.getTripSeconds();
				int id = viajeTemp.getBikeid();
				double distaciaCalculada = calcularDistaciaConIdEstaciones(estacionInicioId, estacionFinalId, stationsS);

				VOBike existe = getBike(id, bikesFromTrips); //SE REVISA SI LA BICICLETA YA EXISTE EN LA LISTA

				if(existe != null) {
					existe.addDistanceToTotalDistancia(distaciaCalculada);
					existe.sumarADuracionViajes(duracionViaje);
					existe.addOneTotalViajes();
				}
				else {
					VOBike bike=new VOBike(id);
					bike.addDistanceToTotalDistancia(distaciaCalculada);
					bike.sumarADuracionViajes(duracionViaje);
					bike.addOneTotalViajes();
					bikesFromTrips.add(bike);
				}
			}
		}

		return bikesFromTrips;
	}

	
	private double calcularDistaciaConIdEstaciones(int estacionInicioId, int estacionFinalId, Stack<VOStation> stationsS) {

		double distanciaCalculada = 0;

		double latitudEstacion1 = 0;
		double longitudEstacion1 = 0;
		double latitudEstacion2 = 0;
		double longitudEstacion2 = 0;

		for(VOStation temp : stationsS) {

			if(temp.darId() == estacionInicioId) {
				latitudEstacion1 = temp.darLatitude();
				longitudEstacion1 = temp.darLongitude();
			}
			else if(temp.darId() == estacionFinalId) {
				latitudEstacion2 = temp.darLatitude();
				longitudEstacion2 = temp.darLongitude();
			}
			if(longitudEstacion2 != 0 && longitudEstacion2 != 0) {
				break;
			}
		}

		if(longitudEstacion2 != 0.0 && longitudEstacion2 != 0.0) {
			distanciaCalculada = this.distance(latitudEstacion1, longitudEstacion1, latitudEstacion2, longitudEstacion2);
		}

		return distanciaCalculada;
	}


	public double distance(double startLat, double startLong,
			double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return EARTH_RADIUS * c; // <-- d
	}


	private static double haversin(double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}


	//TODO Andrea Req 2B
	@Override
	public VOBike[] bikesOrderedByDistanceFromTrips(VOTrip[] trips, Stack<VOStation> estaciones) {
		
		VOBike[] bikes =  this.getBikesListFromTrips(trips, estaciones).toArray(VOBike.class);
		
		Ordenador<VOBike> o = new Ordenador<VOBike>();
		o.ordenar(SortingAlgorithms.MERGESORT, true, bikes, (BikesComparatorByDistance)ComparisonCriteria.COMPARE_BIKES_BY_DISTANCE.getComparator());
		
		return bikes;
		

	}






}
