package model.logic;

public enum FilesRoutes {

	TRIPS_Q1("./data/Divvy_Trips_2017_Q1.csv"),
	TRIPS_Q2("./data/Divvy_Trips_2017_Q2.csv"),
	TRIPS_Q3("./data/Divvy_Trips_2017_Q3.csv"),
	TRIPS_Q4("./data/Divvy_Trips_2017_Q4.csv"),
	TRIPS_Q1_Q4("./data/"),
	STATIONS_Q1_Q2("./data/Divvy_Stations_2017_Q1Q2.csv"),
	STATIONS_Q3_Q4("./data/Divvy_Stations_2017_Q3Q4.csv"),
	BIKE_ROUTES_JSON("./data/CDOT Bike Routes_2014_1216.json");


	private String fileRoute;
	
	
	private FilesRoutes(String ruta) {
		fileRoute = ruta;
	}
	
	public String getFileRoute() {
		return fileRoute;
	}
	
	
	}
