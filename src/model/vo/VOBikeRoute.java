package model.vo;

public class VOBikeRoute implements Comparable<VOBikeRoute>{

	//--------------------------------------------------------
	//Atributos
	//--------------------------------------------------------

	private String bikeRouteType;

	private String location;

	private String referenceStreet;

	private String endStreet1;

	private String endStreet2;

	private double length;


	//--------------------------------------------------------
	//Constructores
	//--------------------------------------------------------

	/*
	 * Constructor vacio
	 */
	public VOBikeRoute() {
		bikeRouteType = "";
		location = "";
		referenceStreet = "";
		endStreet1 = "";
		endStreet2 = "";
		length = 0;
	}

	/*
	 * Construye una ruta con los valores que se pasan por parametro
	 */
	public VOBikeRoute(String bikeRouteType, String route, String referenceStreet, String endStreet1, String endStreet2, double length ) {
		this.bikeRouteType = bikeRouteType;
		this.location = route;
		this.referenceStreet = referenceStreet;
		this.endStreet1 = endStreet1;
		this.endStreet2 = endStreet2;
		this.length = length;
	}

	//--------------------------------------------------------
	//Metodos
	//--------------------------------------------------------


	@Override
	public int compareTo(VOBikeRoute arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	//--------------------------------------------------------
	//Getter y Setters
	//--------------------------------------------------------


	public String getEndStreet2() {
		return endStreet2;
	}

	public void setEndStreet2(String endStreet2) {
		this.endStreet2 = endStreet2;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public String getEndStreet1() {
		return endStreet1;
	}

	public void setEndStreet1(String endStreet1) {
		this.endStreet1 = endStreet1;
	}

	public String getReferenceStreet() {
		return referenceStreet;
	}

	public void setReferenceStreet(String referenceStreet) {
		this.referenceStreet = referenceStreet;
	}

	public String getRoute() {
		return location;
	}

	public void setRoute(String route) {
		this.location = route;
	}

	public String getBikeRouteType() {
		return bikeRouteType;
	}

	public void setBikeRouteType(String bikeRouteType) {
		this.bikeRouteType = bikeRouteType;
	}	

}
