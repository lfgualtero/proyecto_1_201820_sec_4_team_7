package controller;

import java.time.LocalDateTime;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Queue;
import model.logic.DivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOStation;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();

	public static void loadTrips(String trips) {
		manager.loadTrips(trips);
	}

	public static void loadStations(String stations) {
		manager.loadStations(stations);
	}

	public static void loadBikeRoutesJSON() {
		manager.loadBikeRoutesJSON("./data/CDOT Bike Routes_2014_1216.json");

	}

	public static int TripsSSize() {

		return manager.getTripsSSize();
	}

	public static int stationsSSize() {

		return manager.darStationsS().size();
	}

	public static int bikeRoutesSSize() {

		return manager.getBikeRoutes().getSize();
	}

	//---------------------------------------------------------------
	//REQUERIMIENTOS: parte A
	//--------------------------------------------------------------

	public static Queue<VOTrip> A1(LocalDateTime initialDateP, LocalDateTime finalDateP) {
		return manager.getTripsBetweenDates(initialDateP, finalDateP);
	}
	public static DoublyLinkedList<VOBike> A2(LocalDateTime localDateInicio2A, LocalDateTime localDateFin2A) {
		return manager.getBikesBetweenDates(localDateInicio2A, localDateFin2A);
	}

	public static DoublyLinkedList<VOTrip> A3(int idBicicleta3A, LocalDateTime localDateInicio3A, LocalDateTime localDateFin3A) {
		return manager.getABikesTripsBetweenDates(idBicicleta3A, localDateInicio3A, localDateFin3A);
	}
	public static DoublyLinkedList<VOTrip> A4(int idEstacionFinal4A, LocalDateTime localDateInicio4A, LocalDateTime localDateFin4A) {
		return manager.getTripsInfoBetweenDatesSameToStation(idEstacionFinal4A, localDateInicio4A, localDateFin4A);
	}

	//---------------------------------------------------------------
	//REQUERIMIENTOS: parte B
	//--------------------------------------------------------------

	public static Queue<VOStation> B1(LocalDateTime localDateInicio1B) {
		// TODO Auto-generated method stub
		return manager.getStationsFromDate(localDateInicio1B);
	}
	public static VOBike[] B2(LocalDateTime localDateInicio2B, LocalDateTime localDateFin2B) {
		// TODO Auto-generated method stub
		return manager.bikesOrderedByDistanceBetweenDates(localDateInicio2B, localDateFin2B);
	}
	public static VOTrip[] B3(int bicicletaId3B, int tiempoMaximo3B, String genero3b) {
		// TODO Auto-generated method stub
		return manager.getABikeTripsWithShorterDurationThanByGender(bicicletaId3B, tiempoMaximo3B, genero3b);
	}
	public static VOTrip[] B4(int estacionInicioId, LocalDateTime localDateInicio4B, LocalDateTime localDateFin4B) {
		// TODO Auto-generated method stub
		return manager.tripsFromStationBetweenDates(estacionInicioId, localDateInicio4B, localDateFin4B);
	}

	//---------------------------------------------------------------
	//REQUERIMIENTOS: parte C
	//--------------------------------------------------------------
	public static Queue<VOTrip> C2ViajesValidadosBicicleta(int bicicletaId, LocalDateTime localDateInicio2C, LocalDateTime localDateFin2C) {
		return manager.pilaValidacion(bicicletaId, localDateInicio2C, localDateFin2C);
	}
	public static VOBike[] C3BicicletasMasUsadas(int numeroBicicletas3C) {
		// TODO Auto-generated method stub
		return manager.theMostUsedXBikes(numeroBicicletas3C);
	}
	public static Object[] C4ViajesEstacion(int idEstacion4C, LocalDateTime localDateInicio4C, LocalDateTime localDateFin4C) {
		
		return manager.tripsStartedAndFinishedAtASationBetweenDates(idEstacion4C, localDateInicio4C, localDateFin4C);
	}

}
